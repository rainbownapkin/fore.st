fore.st - Pineapple Express += 3 (v1.1.3)
======

fore.st is the server software for ourfore.st, a community based chat & synced video embedding site tailored to service
the TTN community post-shutdown. The softwre is made freely available both for legal reasons
but also as it seems as that is what is best for the community in the advant of another shutdown.

fore.st is a fork of cytube and as such supports all of the same media sources,
and carries many of the same features. Modifcations to the software have been/are 
being made to make the platform a more familiar place for TTN users. Contributions
are welcome.

## Installation
There is currently no installation guide for the software, however since not much has
changed in the backend, you should be fine with official [cytube docs](https://github.com/calzoneman/sync/wiki/CyTube-3.0-Installation-Guide).

## Contact
You can reach out by bugging rainbownapkin on the ttn discord or ourfore.st, you can also send an email to ourforest(at)420blaze.it

## Shoutouts
  - Thanks to Simon for making TTN,
  - Thanks to our excellent mod team for chat moderation and content aggregation
  - Thanks to etchingham for being a community contact while TTN's been winding down, I think all of us see you as a pillar of the community so your support means a lot.
  - Thanks to calzoneman for making [cytube](https://github.com/calzoneman/sync), that saved our asses.
  - Thanks to the core TTN community and everyone else who's ever used it, I was only there for the last handful of years but it was an absolute fuckin' ride. You guys are the best, it isn't TTN but I hope this at least help fills the gap.

## Pineapple Express += 3 1.1.3 Release Notes
 - Add bulk-queueing from Internet Archive

## License
Original fore.st code is provided under the Affero General Public License v3 in order to prevent fore.st being used in proprietary software.
(see the LICENSE file for the full text.)

Cytube source code originally licensed under MIT license
(see the LICENSE file for the full text.)

Bundled source code and assets, such as third-party Images, Audio, Video, CSS and JavaScript libraries, are
provided under their respective licenses.
