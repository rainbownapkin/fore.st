/*
fore.st is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

fore.st is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with fore.st. If not, see < http://www.gnu.org/licenses/ >.
(C) 2022- by rainbownapkin, <ourforest@420blaze.it>
*/

var startTimes = [[],[]]//UID's, StartTimes
var activeItem = 0;//active UID;
var rptime = 0;//reference playlist time
var rltime = 0;//refernce local time(epoch)
var ctime = 0;

const st = new Date();//scratchtime

function dispSTimes(){//update sTimes
	var ptimeString, ltimeString;
	calcRefs();//iterate and findRefs before calcTime
	
	//TODO: replace this with foreach
	//iterate and print
	for(var i = 0; i < startTimes[0].length; i++){//for every item startTime
		var rdif = startTimes[1][i] - rptime;
		st.setTime(rltime + (rdif * 1000));
		ltimeString = "airdate: " + st.toLocaleTimeString().replace(" ","") + " " + st.toLocaleDateString();
		//ptimeString = '<span id="prefTime"> (pref time) ' + formatTime(startTimes[1][i] + '</span>');// create ptimeString
		ptimeString = '(pref time) ' + formatTime(startTimes[1][i]);// create ptimeString

		if($(".pluid-" + startTimes[0][i]) != null || $(".pluid-" + startTimes[0][i]) != undefined){//if current item isnt null or undefined

			$(".pluid-" + startTimes[0][i]).children(".qe_sTime").text(ltimeString);// set current item qe_sTime innerHTML to ptimeString
			$(".pluid-" + startTimes[0][i]).children(".qe_pref").text(ptimeString);// set current item qe_sTime innerHTML to ptimeString

			if($(".pluid-" + startTimes[0][i]) != null && $(".pluid-" + startTimes[0][i]).data("media") != null){
				st.setTime(st.getTime() + ($(".pluid-" + startTimes[0][i]).data("media").seconds * 1000));//calc end time
			}

			$(".pluid-" + startTimes[0][i]).children(".qe_etime").text("enddate: " + st.toLocaleTimeString().replace(" ","") + " " + st.toLocaleDateString());//Set endtime (lil dirty but so is this script :P)

		}
	}
	 
}

function calcRefs(){
	ld = new Date();//localdate
	for(var i = 0; i < startTimes[0].length; i++){//for every item startTime/until activeItem
		if(startTimes[0][i] == activeItem){
			rptime = startTimes[1][i];		
		}
	}
	PLAYER.getTime(function(seek){ctime = seek});
	rltime = ld.getTime() - (ctime * 1000);
}

function expandItem(itm){
	itm.find(".btn-group").show("blind");
	itm.find(".qe_time").show("blind");
	itm.find(".qe_etime").show("blind");
	itm.find(".qe_pref").show("blind");
}

function collapseItem(itm){
	itm.find(".btn-group").hide("blind");
	itm.find(".qe_time").hide("blind");
	itm.find(".qe_etime").hide("blind");
	itm.find(".qe_pref").hide("blind");
}

function toggleItem(itm){
	itm.find(".btn-group").toggle("blind");
	itm.find(".qe_time").toggle("blind");
	itm.find(".qe_etime").toggle("blind");
	itm.find(".qe_pref").toggle("blind");
}

function collapseItems(){
	$.each($("#queue").find("li"), function(i,el){
		collapseItem($(el));
	})
}

function expandItems(){
	$.each($("#queue").find("li"), function(i,el){
		expandItem($(el));
	})
}

function toggleItems(){
	$.each($("#queue").find("li"), function(i,el){
		toggleItem($(el));
	})
}
