/*
fore.st is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

fore.st is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with fore.st. If not, see < http://www.gnu.org/licenses/ >.
(C) 2022- by rainbownapkin, <ourforest@420blaze.it>
*/

var colorList = ["userlist_color0", "userlist_color1", "userlist_color2", "userlist_color3", "userlist_color4", "userlist_color5", "userlist_color6"];

var usrColors = ([[],[]]);

function assignColors(name){
	if(!usrColors[0].includes(name)){
		color = colorList[Math.floor(Math.random()*colorList.length)];
		usrColors[0].push(name);
		usrColors[1].push(color);
	}
}

function freeColor(name){
	if(usrColors[0].includes(name)){
		let itemi = usrColors[0].indexOf(name);
		usrColors[0].splice(itemi, 1);
		usrColors[1].splice(itemi, 1);
	}
}

function getColor(name){
	if((usrColors[0].indexOf(name)) != null){
		return usrColors[1][usrColors[0].indexOf(name)];
	}
	return 0;
}


function chatpaste(str){
	var chatline = 	$("#chatline")[0];
	if(chatline.value === ''){
		chatline.value = str;
	}else{
		chatline.value += ' ' + str;
	}
}

function storeToke(toke){//process and store chats that start with ! for local toke tab completion dictionary
	//let temptoke = JSON.parse(localStorage.getItem("localTokes"));
	let temptoke = loadTokes();//load current toaks into temp var
	if(temptoke === null || temptoke === undefined){//if its babbies first tokem
		temptoke = [];//create var
		if(toke != "!toke"){
			temptoke.push("!toke");//push that shit
		}
		temptoke.push(toke);//push that shit
	}else{//otherwise
		if(!temptoke.includes(toke)){//if it isn't there
			temptoke.push(toke);//add it my d00d
		}
	}

	localStorage.setItem("localTokes", JSON.stringify(temptoke));//slap that sum' bitch back inta the closet
}

function loadTokes(){
	return JSON.parse(localStorage.getItem("localTokes"));//load current toaks into temp var
}

function chatsmack(str){
	var chatline = 	$("#chatline")[0];
	let buf = chatline.value;
	chatline.value = str;
	callChat();
	chatline.value = buf;
}

function chathint(rhint){
	cval = $("#chatline").val();//syntatic sugar :p

	csplit = cval.split(/\s+/);
	hspace = cval.slice(0,cval.length - csplit[csplit.length - 1].length);

	hint = rhint.slice(cval.length);

	if(hint != null && hint != ""){
		$("#chathint").html('<span class="hintspace">' + cval + '</span>' + hint);
	}else{
		$("#chathint").html("");
	}
}

Storage.prototype.setObj = function(key, obj) {
    return this.setItem(key, JSON.stringify(obj))
}
Storage.prototype.getObj = function(key) {
    return JSON.parse(this.getItem(key))
}
