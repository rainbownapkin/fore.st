const commands = new Map([
	["thunder",thunder],
	["lightning",thunder],
]);

function thunder(){
	var lfx = $("<div>").attr("id","lightning");
	var tfx = $("<audio>").append($("<source>").attr("type","audio/ogg").attr("src", window.location.origin + "/audio/thunder" + randrange(0,1) + ".ogg"));

	var lfxinter;
	var flashind = 0;
	var tcount = randrange(2,5);

	$("body").append(lfx.hide());
	$("body").append(tfx.hide());
	tfx.on("ended",function(){
		tfx.remove();
	});

	function showlfx(){
		if(flashind < tcount){
			$("#lightning").show("fade", randrange(10,130),function(){
				$("#lightning").hide("fade", randrange(10,130),function(){
					setTimeout(showlfx, randrange(15 + (flashind * 25),80 + (flashind * 15)));
					flashind++;
				});
			});
		}
	}

	if(!USEROPTS.no_lightning){
		//lfxinter = setInterval(showlfx,randrange(40,200));
		showlfx();
	}

	setTimeout(function(){tfx[0].play()}, randrange(40,60));
}
