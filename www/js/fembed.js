/*
fore.st is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

fore.st is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with fore.st. If not, see < http://www.gnu.org/licenses/ >.
(C) 2022- by rainbownapkin, <ourforest@420blaze.it>
*/

img_ext = ['jpg', 'jpg:large', 'jpeg', 'png', 'tiff', 'gif'];
vid_ext = ['webm', 'gifv', 'mp4'];
proto = ['http', 'https'];

function checkMedia(fname){//check if link points ot media
	var fext = fname.split('.').pop().toLowerCase();//pop file ext
	if(img_ext.includes(fext)){//if its an img
		return 1;
	}else if(vid_ext.includes(fext)){//if its a vid
		return 2;
	}else{//nada
		return 0;
	}
}

function checkEmbed(word, isEmote){
	let regex = /[!"#$%&'()*+,./:;<=>?@[\]^`{|}~]/g;//symbol mask for username
	let tregex = /["#$%&'()*+,-./:;<=>?@[\]^_`{|}~]/g;//symbol mask for tokes
	let cregex = /[!"#$%&'()*+,-.:;<=>?@[\]^_`{|}~]/g;//symbol mask for tokes
	let stripd = word.replace(regex, '');//stripped word for username detection
	let tstripd = word.replace(tregex, '');//stripped word for !toke command detection
	let cstripd = word.replace(cregex, '');//stripped word for /local command detection
	if(word.includes(proto[0]) || word.includes(proto[1])){//check if it starts with a supported proto
		if(checkMedia(word) != 0){//check if media
			return '<img src="' + word + '" class="channel-emote">';//embed media
		}else if(!isEmote){//if its a link
			if(word.includes("imgur.com")){
				if(word.length > 20 && word.length < 28){
					return '<img src="' + word + ".gif" + '" class="channel-emote">';//embed media
				}
			}
			return '<a target="_blank" href="' + word + '">' + word.replace(/.{40}/g, '$& ') + '</a>';//embed link
		}else{//if its an emote
			return word;
		}
	}else if(usrColors[0].includes(stripd)){//if username

			let usersplit = word.split(stripd,2);//split word by stripd, array of before and after name

			return usersplit[0] + '<span id="' + getColor(stripd) + '" onclick="chatpaste(\'' + stripd + '\')">' + stripd + '</span>' + usersplit[1];//decorate name
	}else if(tstripd.charAt(0) === '!' && !isEmote){//if !toke command(same logic as above)
			let tokesplit = word.split(tstripd,2);
			return tokesplit[0] + '<a id="toke" onclick="chatsmack(\'' + tstripd + '\')">' + tstripd + '</a>' + tokesplit[1];
	}else if(cstripd.charAt(0) === '/' && !isEmote){//if local command(same logic as above)
			let cmdsplit = word.split(cstripd,2);
			return cmdsplit[0] + '<a id="toke" onclick="chatsmack(\'' + cstripd + '\')">' + cstripd + '</a>' + cmdsplit[1];
	}else{
		return word.replace(/.{40}/g, '$& ');
	}


}

function findEmbed(str){//check entire string for embeds
	let isEmote = str.includes('class="channel-emote"');
	var splts = str.split(/\s/g);//split string by whitespace
	let baked = checkEmbed(splts[0], isEmote) + ' ';//work string, set first item to checkEmbed of first word
	for(i = 1; i < splts.length; i++){//run through words
			baked += checkEmbed(splts[i], isEmote) + ' ';//checkEmbed() and add to baked with a space after it
	} 
	return baked;//return baked ;p

}
