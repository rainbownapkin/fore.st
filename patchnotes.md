dev goals for 1.1.3 pineapple Express += 3:
 -- Add bulk-queueing from Internet Archive
  - automagically pull most web-compatible version of all videos within a specific upload to IA
  - allow queueing admin to filter directory by video length in minutes
  - allow for bulk naming