/*
fore.st is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

fore.st is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with fore.st. If not, see < http://www.gnu.org/licenses/ >.
(C) 2022- by rainbownapkin, <ourforest@420blaze.it>

Original cytube license:
MIT License

Copyright (c) 2013-2022 Calvin Montgomery

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

const https = require("https");
const Media = require("./media");
const CustomEmbedFilter = require("./customembed").filter;
const Config = require("./config");
const XSS = require("./xss");
const ffmpeg = require("./ffmpeg");
const mediaquery = require("@cytube/mediaquery");
const YouTube = require("@cytube/mediaquery/lib/provider/youtube");
const Vimeo = require("@cytube/mediaquery/lib/provider/vimeo");
const Streamable = require("@cytube/mediaquery/lib/provider/streamable");
const TwitchVOD = require("@cytube/mediaquery/lib/provider/twitch-vod");
const TwitchClip = require("@cytube/mediaquery/lib/provider/twitch-clip");
const YTDLP = require('youtube-dl-exec');

import { Counter } from 'prom-client';
import { lookup as lookupCustomMetadata } from './custom-media';

const LOGGER = require('@calzoneman/jsli')('get-info');
const lookupCounter = new Counter({
    name: 'cytube_media_lookups_total',
    help: 'Count of media lookups',
    labelNames: ['shortCode']
});

var urlRetrieve = function (transport, options, callback) {
    var req = transport.request(options, function (res) {
        res.on("error", function (err) {
            LOGGER.error("HTTP response " + options.host + options.path + " failed: "+
                err);
            callback(503, "");
        });

        var buffer = "";
        res.setEncoding("utf-8");
        res.on("data", function (chunk) {
            buffer += chunk;
        });
        res.on("end", function () {
            callback(res.statusCode, buffer);
        });
    });

    req.on("error", function (err) {
        LOGGER.error("HTTP request " + options.host + options.path + " failed: " +
            err);
        callback(503, "");
    });

    req.end();
};

var mediaTypeMap = {
    "youtube": "yt",
    "googledrive": "gd",
    "google+": "gp"
};

function convertMedia(media) {
    return new Media(media.id, media.title, media.duration, mediaTypeMap[media.type],
            media.meta);
}

function getBlocked(reg){
	var regionlist = ["AD","AE","AF","AG","AI","AL","AM","AO","AQ","AR","AS","AT","AU","AW","AX","AZ","BB","BD","BE","BF","BG","BH","BI","BJ","BL","BM","BN","BO","BR","BS","BT","BV","BW","BY","BZ","CA","CC","CD","CF","CG","CH","CI","CK","CL","CM","CN","CO","CR","CU","CV","CX","CY","CZ","DE","DJ","DK","DM","DO","DZ","EC","EE","EG","EH","ER","ES","ET","FI","FJ","FK","FM","FO","FR","GA","GB","GD","GE","GF","GG","GH","GI","GL","GM","GN","GP","GQ","GR","GS","GT","GU","GW","GY","HK","HM","HN","HR","HT","HU","ID","IE","IL","IM","IN","IO","IQ","IR","IS","IT","JE","JM","JO","JP","KE","KG","KH","KI","KM","KN","KP","KR","KW","KY","KZ","LA","LB","LC","LI","LK","LR","LS","LT","LU","LV","LY","MA","MC","MD","ME","MG","MH","MK","ML","MM","MN","MO","MP","MQ","MR","MS","MT","MU","MV","MW","MX","MY","MZ","NA","NC","NE","NF","NG","NI","NL","NO","NP","NR","NU","NZ","OM","PA","PE","PF","PG","PH","PK","PL","PM","PN","PR","PS","PT","PW","PY","QA","RE","RO","RS","RU","RW","SA","SB","SC","SD","SE","SG","SH","SI","SJ","SK","SL","SM","SN","SO","SR","ST","SV","SY","SZ","TC","TD","TF","TG","TH","TJ","TK","TL","TM","TN","TO","TR","TT","TV","TW","TZ","UA","UG","UM","US","UY","UZ","VA","VC","VE","VG","VI","VN","VU","WF","WS","YE","YT","ZA","ZM","ZW"];//forgive me father, for I have sinned.

	var blck = [];
	for(var i = 0; i < regionlist.length; i++){
		if(!reg.includes(regionlist[i])){
			blck.push(regionlist[i]);
		}
	}
	return blck;
}

var Getters = {
    /* youtube.com */
    yt: async function (id, callback) {

	if(!Config.get("invidious-backend")){//legacy youtube backend (fucking yicky)
		if (!Config.get("youtube-v3-key")) {
		    return callback("The YouTube API now requires an API key. You could sign up for an API key, but you're a lot better off using the invidious backend!" + 
		    "See your config.yaml for the deets.");
		}


		YouTube.lookup(id).then(function (video) {
		    var meta = {};
		    if (video.meta.blocked) {
			meta.restricted = video.meta.blocked;
		    }
		    if (video.meta.ytRating) {
			meta.ytRating = video.meta.ytRating;
		    }

		    var media = new Media(video.id, video.title, video.duration, "yt", meta);
		    callback(false, media);
		}).catch(function (err) {
		    callback(err.message || err, null);
		});
	}else{
        //yt-dlp calls (google bad)
        try{
            var video = await YTDLP(id,{
                    dumpSingleJson: true,
                }
            )
            
            var meta = {
                ytRating: video.like_count
            }

            var media = new Media(video.id, video.title, video.duration, "yt", meta);


            callback(false, media);
        }catch(err){
            callback(err.message || err, null);
        }

        //callback("test funciton, remove this call!" || err, null);
        //invidious api calls (o7 Stream on, you magnificent bastard. We'll always remember you! <3 2018-2014)
		/*var options = {
		    host: Config.get("invidious-source"),
		    port: 443,
		    path: "/api/v1/videos/" + id,
		    method: "GET",
		    timeout: 1000
		};

		urlRetrieve(https, options, function (status, data) {
			if(status !== 200) {
				return callback("Invidious HTTPS error code: " + status, null);
			}

			var vid = JSON.parse(data);
			var meta = {}

			if(getBlocked(vid.allowedRegions).length > 0){
				meta.restricted = getBlocked(vid.allowedRegions);
			}

			if(vid.likeCount){
				meta.ytRating = vid.likeCount;
			}

			var media = new Media(vid.videoId, vid.title, vid.lengthSeconds, "yt", meta);
			return callback(false, media);
		});*/

	}
    },

    /* youtube.com playlists */
    yp: async function (id, callback) {
	if(!Config.get("invidious-backend")){//legacy youtube backend (fucking yicky)
		if (!Config.get("youtube-v3-key")) {
		    	    return callback("The YouTube API now requires an API key. You could sign up for an API key, but you're a lot better off using the invidious backend!" + 
		    "See your config.yaml for the deets.");
		}

		YouTube.lookupPlaylist(id).then(function (videos) {
		    videos = videos.map(function (video) {
			var meta = {};
			if (video.meta.blocked) {
			    meta.restricted = video.meta.blocked;
			}

			return new Media(video.id, video.title, video.duration, "yt", meta);
		    });

		    callback(null, videos);
		}).catch(function (err) {
		    callback(err.message || err, null);
		});
	}else{//yt-dlp wrapper calls (google bad)
        try{
            var list = await YTDLP(id,{
                    dumpSingleJson: true,
                }
            );

            var videos = new Array;

            list.entries.forEach(function(video){
                if(video != null){
                    var meta = {
                        ytRating: video.like_count
                    }

                    videos.push(new Media(video.id, video.title, video.duration, "yt", meta));
                }
            });


            callback(null, videos);
        }catch(err){
            callback(err.message || err, null);
        }       


        /*invidious api calls (o7 RIP 2018-2024)
		var options = {
		    host: Config.get("invidious-source"),
		    port: 443,
		    path: "/api/v1/playlists/" + id,
		    method: "GET",
		    timeout: 1000
		};

		urlRetrieve(https, options, function (status, data) {
			if(status !== 200) {
				return callback("Invidious HTTPS error code: " + status, null);
			}

			var pl = JSON.parse(data).videos;

			var vids = pl.map(function(vid){
				return new Media(vid.videoId, vid.title, vid.lengthSeconds, "yt", []);//return the vid as media obj, (skip out on meta to avoid extra api calls)
			});

			return callback(null, vids);
		});*/
	}
    },

    /* youtube.com search */
    ytSearch: async function (query, callback) {

	if(!Config.get("invidious-backend")){//legacy youtube backend (fucking yicky)
		if (!Config.get("youtube-v3-key")) {
		    return callback("The YouTube API now requires an API key.  Please see the " +
					    "documentation for youtube-v3-key in config.template.yaml");
		}

		YouTube.search(query).then(function (res) {
		    var videos = res.results;
		    videos = videos.map(function (video) {
			var meta = {};
			if (video.meta.blocked) {
                    meta.restricted = video.meta.blocked;
			}

			var media = new Media(video.id, video.title, video.duration, "yt", meta);
			media.thumb = { url: video.meta.thumbnail };
			return media;
		    });

		    callback(null, videos);
		}).catch(function (err) {
		    callback(err.message || err, null);
		});
	}else{

        try{
            var results = await YTDLP(`ytsearch8:${query}`,{
                    dumpSingleJson: true,
                }
            );

            var videos = new Array;

            results.entries.forEach(function(result){
                var meta = {
                    ytRating: result.like_count
                }

                var video = new Media(result.id, result.title, result.duration, "yt", meta);
                video.thumb = {url: result.thumbnails[5].url};

                videos.push(video);
            });

            callback(null, videos);
        }catch(err){
            callback(err.message || err, null);
        }

        /*invidious api calls (o7 RIP 2018-2024)
		var options = {
		    host: Config.get("invidious-source"),
		    port: 443,
		    path: "/api/v1/search?q='" + encodeURI(XSS.sanitizeText(query)) + "'",
		    method: "GET",
		    timeout: 1000
		};

		urlRetrieve(https, options, function (status, data) {
			if(status !== 200) {
				return callback("Invidious HTTPS error code: " + status, null);
			}

			var srch = JSON.parse(data);
			var vids = srch.map(function(rslt, i){
				var media;
				if(rslt.type === "video"){
					media = new Media(rslt.videoId, rslt.title, rslt.lengthSeconds, "yt", [])//create new media object from curent rslt
					media.thumb = {url: rslt.videoThumbnails[5].url};
					return media
				}
			});

			return callback(null, vids.filter(rs => rs != null));
		});*/
	}
    },

    /* vimeo.com */
    vi: function (id, callback) {
        var m = id.match(/([\w-]+)/);
        if (m) {
            id = m[1];
        } else {
            callback("Invalid ID", null);
            return;
        }

        Vimeo.lookup(id).then(video => {
            video = new Media(video.id, video.title, video.duration, "vi");
            callback(null, video);
        }).catch(error => {
            callback(error.message);
        });
    },

    /* dailymotion.com */
    //The dailymotion player has been broken, however their basic API remains intact.
    //This will stay *for now* but will be replaced by yt-dlp at the first sign of resistance.
    dm: function (id, callback) {
        var m = id.match(/([\w-]+)/);
        if (m) {
            id = m[1].split("_")[0];
        } else {
            callback("Invalid ID", null);
            return;
        }
        var options = {
            host: "api.dailymotion.com",
            port: 443,
            path: "/video/" + id + "?fields=duration,title",
            method: "GET",
            dataType: "jsonp",
            timeout: 1000
        };

        urlRetrieve(https, options, function (status, data) {
            switch (status) {
                case 200:
                    break; /* Request is OK, skip to handling data */
                case 400:
                    return callback("Invalid request", null);
                case 403:
                    return callback("Private video", null);
                case 404:
                    return callback("Video not found", null);
                case 500:
                case 503:
                    return callback("Service unavailable", null);
                default:
                    return callback("HTTP " + status, null);
            }

            try {
                data = JSON.parse(data);
                var title = data.title;
                var seconds = data.duration;
                /**
                 * This is a rather hacky way to indicate that a video has
                 * been deleted...
                 */
                if (title === "Deleted video" && seconds === 10) {
                    callback("Video not found", null);
                    return;
                }
                var media = new Media(id, title, seconds, "dm");
                callback(false, media);
            } catch(e) {
                callback(e, null);
            }
        });
    },

    /* soundcloud.com - see https://github.com/calzoneman/sync/issues/916 */
    sc: function (id, callback) {
        callback(
            "Soundcloud is not supported anymore due to requiring OAuth but not " +
            "accepting new API key registrations."
        );
    },

    /* livestream.com */
    li: function (id, callback) {
        var m = id.match(/([\w-]+)/);
        if (m) {
            id = m[1];
        } else {
            callback("Invalid ID", null);
            return;
        }
        var title = "Livestream.com - " + id;
        var media = new Media(id, title, "--:--", "li");
        callback(false, media);
    },

    /* twitch.tv */
    tw: function (id, callback) {
        var m = id.match(/([\w-]+)/);
        if (m) {
            id = m[1];
        } else {
            callback("Invalid ID", null);
            return;
        }
        var title = "Twitch.tv - " + id;
        var media = new Media(id, title, "--:--", "tw");
        callback(false, media);
    },

    /* twitch VOD */
    tv: function (id, callback) {
        var m = id.match(/([cv]\d+)/);
        if (m) {
            id = m[1];
        } else {
            process.nextTick(callback, "Invalid Twitch VOD ID");
            return;
        }

        TwitchVOD.lookup(id).then(video => {
            const media = new Media(video.id, video.title, video.duration,
                                    "tv", video.meta);
            process.nextTick(callback, false, media);
        }).catch(function (err) {
            callback(err.message || err, null);
        });
    },

    /* twitch clip */
    tc: function (id, callback) {
        var m = id.match(/^([A-Za-z]+)$/);
        if (m) {
            id = m[1];
        } else {
            process.nextTick(callback, "Invalid Twitch VOD ID");
            return;
        }

        TwitchClip.lookup(id).then(video => {
            const media = new Media(video.id, video.title, video.duration,
                                    "tc", video.meta);
            process.nextTick(callback, false, media);
        }).catch(function (err) {
            callback(err.message || err, null);
        });
    },

    /* ustream.tv */
    us: function (id, callback) {
        var m = id.match(/(channel\/[^?&#]+)/);
        if (m) {
            id = m[1];
        } else {
            callback("Invalid ID", null);
            return;
        }

        var options = {
            host: "www.ustream.tv",
            port: 443,
            path: "/" + id,
            method: "GET",
            timeout: 1000
        };

        urlRetrieve(https, options, function (status, data) {
            if(status !== 200) {
                callback("Ustream HTTP " + status, null);
                return;
            }

            /*
             * Yes, regexing this information out of the HTML sucks.
             * No, there is not a better solution -- it seems IBM
             * deprecated the old API (or at least replaced with an
             * enterprise API marked "Contact sales") so fuck it.
             */
            var m = data.match(/https:\/\/www\.ustream\.tv\/embed\/(\d+)/);
            if (m) {
                var title = "Ustream.tv - " + id;
                var media = new Media(m[1], title, "--:--", "us");
                callback(false, media);
            } else {
                callback("Channel ID not found", null);
            }
        });
    },

    /* rtmp stream */
    rt: function (id, callback) {
        var title = "Livestream";
        var media = new Media(id, title, "--:--", "rt");
        callback(false, media);
    },

    /* HLS stream */
    hl: function (id, callback) {
        if (!/^https/.test(id)) {
            callback(
                "HLS links must start with HTTPS due to browser security " +
                "policy.  See https://git.io/vpDLK for details."
            );
            return;
        }
        var title = "Livestream";
        var media = new Media(id, title, "--:--", "hl");
        callback(false, media);
    },

    /* custom embed */
    cu: function (id, callback) {
        var media;
        try {
            media = CustomEmbedFilter(id);
        } catch (e) {
            if (/invalid embed/i.test(e.message)) {
                return callback(e.message);
            } else {
                LOGGER.error(e.stack);
                return callback("Unknown error processing embed");
            }
        }
        callback(false, media);
    },

    /* google docs */
    gd: function (id, callback) {
        if (!/^[a-zA-Z0-9_-]+$/.test(id)) {
            callback("Invalid ID: " + id);
            return;
        }

        var data = {
            type: "googledrive",
            kind: "single",
            id: id
        };

        mediaquery.lookup(data).then(function (video) {
            callback(null, convertMedia(video));
        }).catch(function (err) {
            callback(err.message || err);
        });
    },

    /* ffmpeg for raw files */
    fi: function (id, cb) {
        ffmpeg.query(id, function (err, data) {
            if (err) {
                return cb(err);
            }

            var m = new Media(id, data.title, data.duration, "fi", {
                bitrate: data.bitrate,
                codec: data.codec
            });
            cb(null, m);
        });
    },

    /* hitbox.tv / smashcast.tv */
    hb: function (id, callback) {
        var m = id.match(/([\w-]+)/);
        if (m) {
            id = m[1];
        } else {
            callback("Invalid ID", null);
            return;
        }
        var title = "Smashcast - " + id;
        var media = new Media(id, title, "--:--", "hb");
        callback(false, media);
    },

    /* vid.me */
    vm: function (id, callback) {
        process.nextTick(
            callback,
            "As of December 2017, vid.me is no longer in service."
        );
    },

    /* streamable */
    sb: function (id, callback) {
        if (!/^[\w-]+$/.test(id)) {
            process.nextTick(callback, "Invalid streamable.com ID");
            return;
        }

        Streamable.lookup(id).then(video => {
            const media = new Media(video.id, video.title, video.duration,
                                    "sb", video.meta);
            process.nextTick(callback, false, media);
        }).catch(function (err) {
            callback(err.message || err, null);
        });
    },

    /* custom media - https://github.com/calzoneman/sync/issues/655 */
    cm: async function (id, callback) {
        try {
            const media = await lookupCustomMetadata(id);
            process.nextTick(callback, false, media);
        } catch (error) {
            process.nextTick(callback, error.message);
        }
    },

    /* mixer.com */
    mx: function (id, callback) {
        process.nextTick(
            callback,
            "As of July 2020, Mixer is no longer in service."
        );
    },

    /*Internet Archive Bulk Grabber*/
    ia: function(id, minDuration, callback){
        try{
        //Get metadata on the directory
        var options = {
            host: "archive.org",
            port: 443,
            path: "/metadata/" + id,
            method: "GET",
            timeout: 1000
        };

        //pull the URL
        urlRetrieve(https, options, function (status, data) {
            //if we fucked up
            if(status !== 200) {
                return callback("Archive.org HTTPS error code: " + status, null);
            }
            
            //Parse the dump
            var dump = JSON.parse(data);


            //if we have files
            if(dump.files != null){
                var vids = new Array;
                var derivative = new Array;
                var media = new Array;

                //sift through files to find .mp4's
                dump.files.forEach(function(file){
                    //Skip out on videos that dont meet the requested minimum duration
                    if(file.length >= minDuration){
                        //if its a standard .mp4 (either MPEG, or h.264)
                        if(file.format == "h.264" || file.format == "MPEG4"){
                            //add the file to the video array
                            vids.push(file);
                        //if it's been transcoded by archive.org to ensure web-compatibility
                        }else if(file.format == "h.264 IA"){
                            //add the file to the derivative array
                            derivative.push(file);
                        }
                    }
                });

                //chose derivatives over originals to save on bandwith and ensure web-compatibility, even if they don't always look as nice :P
                derivative.forEach(function(file){
                    //sift through standard files to find matching originals
                    for(var i = 0; i < vids.length; i++){
                        //if we have a match
                        if(vids[i].name == file.original){
                            //replace the file with the correct derivative
                            vids[i] = file;
                        }
                    }
                });

                //Lets try this just using info from IA's api. It would take up way less fucking time, even if we can't fill in everything...
                vids = vids.map(function (file) {
                    return new Media(`https://${dump.d1}${dump.dir}/${file.name}`, dump.metadata.title, file.length, "fi", {codec: "mov/h264"});
                });

                process.nextTick(callback, false, vids);

            //if we fucked up some other way
            }else if(dump.error != null){
                return callback(`Archive.org error: ${dump.error}`);
            }else{
                return callback("Unkown metadata error from archive.org!");
            }
        });

        } catch (err) {
            callback(err.message);
        }
    }
};

module.exports = {
    Getters: Getters,
    getMedia: function (id, type, callback, minDuration) {
        if (type in this.Getters) {
        LOGGER.info("Looking up %s:%s", type, id);
        lookupCounter.labels(type).inc(1, new Date());
        if(type == "ia"){
            this.Getters.ia(id, minDuration, callback);
        }else{
            this.Getters[type](id, callback);
        }
        } else {
        callback("Unknown media type '" + type + "'", null);
        }
    },
	getRawCopy: async function (id, cb){
        try{
            var video = await YTDLP(id,{
                    dumpSingleJson: true,
                }
            )

            cb(video.requested_downloads[0].url);
        }catch(err){
            console.log(err.message);
        }
    /* Invidious Code (o7 RIP 2018-2024)
		var options = {
				host: Config.get("invidious-source"),
				port: 443,
				path: "/api/v1/videos/" + id,
				method: "GET",
				timeout: 1000
		};

		urlRetrieve(https, options, function (status, data) {
            
         old invidious code (o7 2018 - 2024)
			if(status !== 200) {
				console.log("Invidious HTTPS error code: " + status);
			}

			var vid = JSON.parse(data);

			if(vid.formatStreams[0] != null){//TEMPORARY FOR FRONTEND DEV PURPOSES, PULL LINK AND SET AGAIN WHEN VIDEO QUEUED(shit expires)
				cb(vid.formatStreams[vid.formatStreams.length - 1].url);
			}
        
		});*/
	}
};
