import { sendPug } from '../pug';

export default function initialize(app) {
    app.get('/about', (req, res) => {
        return sendPug(res, 'about');
    });
}
