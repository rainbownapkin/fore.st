var ChannelModule = require("./module");
var Flags = require("../flags");

function AnonymousCheck(_channel) {
    ChannelModule.apply(this, arguments);
}

AnonymousCheck.prototype = Object.create(ChannelModule.prototype);

AnonymousCheck.prototype.onUserPreJoin = function (user, data, cb) {
    const opts = this.channel.modules.options;
    var anonymousBanned =  opts.get("block_anonymous_users");

    if (user.socket.disconnected) {
        return cb("User disconnected", ChannelModule.DENY);
    }

    if(/*anonymousBanned &&*/ user.isAnonymous()) {//we're just dropping the config check for now, but anonymous users shouldnt be allowed
        user.socket.on("disconnect", function () {
            if (!user.is(Flags.U_IN_CHANNEL)) {
                cb("User disconnected", ChannelModule.DENY);
            }
        });

        user.socket.emit("errorMsg", { msg : "Welcome to ourfore.st! Register to start chatting/streaming!"});
	user.socket.disconnect();

        user.waitFlag(Flags.U_LOGGED_IN, function () {
            cb(null, ChannelModule.PASSTHROUGH);
        });
        return;
    } else{
        cb(null, ChannelModule.PASSTHROUGH);
    }
};

module.exports = AnonymousCheck;
