/*
fore.st is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

fore.st is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with fore.st. If not, see < http://www.gnu.org/licenses/ >.
(C) 2022- by rainbownapkin, <ourforest@420blaze.it>
*/
import fs from 'fs';

const InfoGetter = require ("../get-info");
const XSS = require("../xss");
const ChannelModule = require("./module");
const Media = require("../media");
const util = require("../utilities");
const Server = require("../server");

//type declerations
const TYPE_NEWBUMP = {
	name: "string",
	user: "string",
	id: "string",
	type: "string",
	rtoke: "boolean",
	bumplist: "string",
	noauto: "boolean"
};

const TYPE_DELBUMP = {
	bl: "string",
	id: "number"
}

const TYPE_SELTYPE = {
	bump: "number",
	list: "number"
}

const TYPE_BFREQ = {
	min: "number",
	max: "number"
}

const TYPE_RENAME = {
	bl: "string",
	id: "number",
	name: "string"
}

const TYPE_LISTRENAME = {
	oldname: "string",
	newname: "string"
}

//global vars
var bumplists = null;//decalare variable but keep it null until lists are loaded
var lowerReg = /[\s!"#$%&'()*+,./:;<=>?@[\]^`{|}~]/g;//regex for ripping out specials and whitespace from lowernames
var bumpFolder = "bumps/"
var configFolder = bumpFolder + "config/"

//global functions
function randi(len) {//get random number from zero to len, meant for use to pull random items from an array
	return Math.floor(Math.random() * len); //The maximum is exclusive and the minimum is inclusive
}

function randrange(min,max){
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is exclusive and the minimum is inclusive
}

function loadList(bfile){
	var _this = this;
	fs.readFile(bfile, function(err,rdata){
		if(err){
			console.log("[loadlist]  " + err);
			return;
		}	
		
		var data = JSON.parse(rdata);
		var blist = [];
		data.bumps.forEach(function(b){
			if(b != null){
				var nbump = new bump(b.name, b.user, b.rtoke, b.media, null, b.noauto);
				nbump.listname = data.lowername;
				nbump.id = b.id;
				blist[b.id] = nbump;
			}
		});
		
		bumplists.set(data.lowername, new bumplist(data.name,blist));
	});
}

function loadLists(cb, callp){
	fs.readdir(bumpFolder, function(err, item){
		if(err){
			console.log("[loadlists] " + err);
		}

		bumplists = new Map();//create new map to load lists into, this clears the variable as well as lets the channel know whether or not they have been loaded yet.

		if(item != null){
			item.forEach(function(list, i){
				if(list != configFolder.slice(bumpFolder.length, configFolder.length - 1) && list.slice(list.length - 5) === ".bump"){
					loadList("bumps/" + list);
				}
			});
		}
		
		if(cb != null){//lil' nasty but it calls loadconfig after loading lists :P
			if(callp != null){
				cb(callp);
			}else{
				cb();
			}
		}

	});
}


//constructors
function bump(name, user, rtoke, media, bumplist, noauto){//bump object constructor
	this.name = name;
	this.user = user;
	this.lowername = name.toLowerCase().replace(lowerReg, "");
	this.rtoke = rtoke;
	this.id = null;//this is assigned by the bumplist :P
	this.media = media;
	this.noauto = noauto
	if(bumplist != null){
		if(bumplists.get(bumplist.toLowerCase().replace(lowerReg, ""))){//if bumplist exists
			bumplists.get(bumplist.toLowerCase().replace(lowerReg, "")).addBump(this);//add this to the bumplist
		}
	}
}

function bumplist(name, bumps){//bumplist object constructor
	this.name = name;
	this.lowername = this.name.toLowerCase().replace(lowerReg, "");
	this.bumps = (bumps == null ? [] : bumps);
}

//prototypes
//bump
bump.prototype.rename = function(name){
	this.name = name;
	this.lowername = this.name.toLowerCase().replace(lowerReg, "");
}

//bumplist
bumplist.prototype.saveList = function(oldname){
	var _this = this;
	fs.writeFile("bumps/" + this.lowername + ".bump", JSON.stringify(this), function(err,data){ //RIPPED FROM TOKEBOT, NOT CHANGED YET
		if(err){
			console.log("[bump] BUMP LIST " + _this.name + " FILE WRITE ERROR: " + err);
		}

			Server.getServer().channels.forEach(function(channel){
				channel.users.forEach(function(user){
					if(user != null && user.account.effectiveRank >= 2){
						channel.modules.autobump.sendList(user, _this.lowername, oldname);
					}
				});
			});
	});
};

bumplist.prototype.addBump = function(bump){//add bump to bumplist
	bump.id = (this.getLastId());//Set ID to the ID of the last item + 1 unless there is no item
	bump.listname = this.lowername;

	this.bumps[bump.id] = bump;//add bump at bump id
	this.saveList();
};

bumplist.prototype.deleteBump = function(bump){//delete bumps (should probably adjust ids and index of bumps but meh)
	if(bump != null){
		this.bumps[bump.id] = null;//null out bump
		this.saveList();//save the bitch
	}

};

bumplist.prototype.getLastId = function(){
	var lid = 0;
	this.bumps.forEach(function(bump){
		if(bump != null){
			lid = bump.id + 1;
		}
	});
	return lid;
}

bumplist.prototype.packList = function(){
	var pbl = {
		name: this.name,
		lowername: this.lowername,
		bumps: []
	}

	this.bumps.forEach(function(bump){
		if(bump != null){
			pbl.bumps[bump.id] = {
				name: bump.name,
				lowername: bump.lowername,
				rtoke: bump.rtoke,
				id: bump.id,
				noauto: bump.noauto,
				media: {
					id: bump.media.id,
					title: bump.media.title,
					type: bump.media.type,
					duration: bump.media.duration
				}
			}

			if(bump.user != null){
				pbl.bumps[bump.id].user = bump.user;
			}
		}
	});

	return pbl;
}

bumplist.prototype.rename = function(oldname,nname,cb){
		var _this = this;
		var lnname = nname.toLowerCase().replace(lowerReg, "");

		fs.rename("bumps/" + this.lowername + ".bump", "bumps/" + lnname + ".bump", function(err){
			if(err){
				console.log("[bump] BUMP LIST " + _this.name + " FILE RENAME ERROR: " + err);
			}

			bumplists.delete(_this.lowername);

			_this.name = nname;
			_this.lowername = lnname;

			_this.bumps.forEach(function(bump){
				if(bump != null){
					bump.listname = _this.lowername;
				}
			});

			bumplists.set(_this.lowername, _this);//create new bumplist

			_this.saveList(oldname);
			if(typeof cb === "function"){
				cb();
			}
		});
}

bumplist.prototype.deleteList = function(){
		var _this = this;
		fs.unlink("bumps/" + this.lowername + ".bump", function (err){
			if(err){
				console.log("[bump] BUMP LIST " + _this.name + " FILE DELETE ERROR: " + err);
			}

			Server.getServer().channels.forEach(function(channel){
				channel.users.forEach(function(user){
					if(user != null && user.account.effectiveRank >= 2){
						user.socket.emit("rmBumplist",_this.lowername);
					}
				});
			});

			bumplists.delete(_this.lowername);		
		});
};

//constructor
function AutobumpModule(_channel){
	ChannelModule.apply(this, arguments);
	this.selmed = this.lastHalfRandom;
	this.listsel = this.smashList;
	if(bumplists == null){
		loadLists(this.loadConfig, this);//on startup load lists
	}else{
		this.loadConfig();
	}
}

//module protoype definition
AutobumpModule.prototype = Object.create(ChannelModule.prototype);
AutobumpModule.prototype.lastPlayed = [];
AutobumpModule.prototype.activeLists = new Map();

//event handling
AutobumpModule.prototype.onUserPostJoin = function (user){//on user join
	if(user.account.effectiveRank >= 2){
		user.socket.typecheckedOn("newBump", TYPE_NEWBUMP, this.handleNewBump.bind(this, user));//handle newBump
		user.socket.typecheckedOn("deleteBump", TYPE_DELBUMP, this.handleDeleteBump.bind(this, user));//handle newBumplist
		user.socket.typecheckedOn("renameBump", TYPE_RENAME, this.handleRenameBump.bind(this, user));//handle newBumplist
		user.socket.typecheckedOn("changeCreator", TYPE_RENAME, this.handleChangeCreator.bind(this, user));//handle newBumplist
		user.socket.typecheckedOn("toggleRtoke", TYPE_DELBUMP, this.handleToggleRtoke.bind(this, user));//toggle rtoke
		user.socket.typecheckedOn("toggleNoauto", TYPE_DELBUMP, this.handleToggleNoauto.bind(this, user));//toggle rtoke
		user.socket.typecheckedOn("newBumplist", '', this.handleNewBumplist.bind(this, user));//handle newBumplist
		user.socket.typecheckedOn("cloneBumplist", '', this.handleCloneBumplist.bind(this, user));//handle newBumplist
		user.socket.typecheckedOn("delBumplist", '', this.handleDelBumplist.bind(this, user));//handle newBumplist
		user.socket.typecheckedOn("renameBumplist", TYPE_LISTRENAME, this.handleRenameBumplist.bind(this, user));//handle newBumplist
		user.socket.typecheckedOn("setActive", '', this.handleSetActive.bind(this, user));//handle newBumplist
		user.socket.typecheckedOn("removeActive", '', this.handleRemoveActive.bind(this, user));//handle newBumplist
		user.socket.typecheckedOn("queueBump", TYPE_DELBUMP, this.handleQueueBump.bind(this, user));//TODO:fix perms for this 
		user.socket.typecheckedOn("setSelect", TYPE_SELTYPE, this.handleSetSelect.bind(this, user));//TODO:fix perms for this 
		user.socket.typecheckedOn("setAgro", 0, this.handleSetAgro.bind(this, user));//TODO:fix perms for this 
		user.socket.typecheckedOn("setBumpFreq", TYPE_BFREQ, this.handleSetBumpFreq.bind(this, user));//TODO:fix perms for this 
		user.socket.typecheckedOn("setMinBump", 0, this.handleSetMinBump.bind(this, user));//TODO:fix perms for this 
		user.socket.on("getBumplists", this.sendLists.bind(this, user));//send lists
		
		this.sendLists(user);	
	}
};

AutobumpModule.prototype.onMediaChange = function(data){
	if(this.agro >= 1){//if agro is 1 or above
		var curFound = false;
		var _this = this;
		this.channel.modules.playlist.items.forEach(function(vid){
			if(!curFound){//if item is not found
				if(_this.channel.modules.playlist.current.prev != null){//if last item isnt null
					curFound = (_this.channel.modules.playlist.current.prev.uid == vid.uid);//check if we're looking at current item
					if(vid.media.isBump){//if the bitch is a bump
						_this.channel.modules.playlist._delete(vid.uid);//FUCKIN KILL IT, KILL IT WITH FIRE!
					}
				}else{//if this goes then its probably the first item in the playlist, no reason to check for old bumps
					curFound = true;
				}
			}//leave new items alone
		});

		var nextp = this.channel.modules.playlist.current.next != null //presence of next item
		var nextb = false;//if next item is bump

		if(nextp){//if there is something next
			nextb = this.channel.modules.playlist.current.next.media.isBump;//check if next item is bump
			if(!nextb && !data.isBump){//if neither current nor next item is bump
				this.autobump();//queue bump via selmed
			}
		}else{//otherwise
			this.autobump();//queue bump via selmed
		}
	}
};

AutobumpModule.prototype.onMediaAdd = function(data, media){
	if(this.agro >= 2){//if agro is 2 or above
		if(data.pos === "next"){//if someone added something next
			if(!media.isBump){//new item isn't bumps
				if(this.channel.modules.playlist.current.next != null){
					var lastBump = this.findBlockEnd(this.channel.modules.playlist.current.next.next);//get the last bump of the block
					if(lastBump != null){//if we got a bump in the block
						if(lastBump.media != media && lastBump != this.channel.modules.playlist.current){//make sure we actually have a block and this isn't returning the currently playing item or the item we added
							var moved = {//create move obj
							    from: this.channel.modules.playlist.current.next.uid,//set from as the item that was just added
							    after: lastBump.uid,//move item after block
							    sTimes: [[],[]]
							};

							this.channel.modules.playlist.handleMoveMedia("autobump",moved,true);
						}
					}
				}
			}
		}
	}
}

AutobumpModule.prototype.onMediaMove = function(data){
	if(this.agro >= 3){//if agro is 3 or above
		if(data.after === "prepend"){
			return;//can't skip bumps this way anyhow			
		}

		var lastBump = this.findBlockEnd();//get last item of (potentially cut) block of bumps
		var nItem = this.channel.modules.playlist.items.find(data.after).next.next;//get item after the moved item

		//if (data.after == lastbump.id) or (data.after === current.id) and  next item is a bump and current item isn't a bump
		if((data.after == lastBump.uid || data.after == this.channel.modules.playlist.current.uid) && 
		(nItem != null) && (nItem.media.isBump) && !nItem.prev.media.isBump){
			var eBlock = this.findBlockEnd(nItem);//get last bump from block of bumps queued after moved item

			var moved = {//create move obj
			    from: nItem.prev.uid,//set from as the item that was just moved
			    after: eBlock.uid,//move item after block
			    sTimes: [[],[]]
			};

			this.channel.modules.playlist.handleMoveMedia("autobump",moved,true);

		}
	}
};

//Utility
AutobumpModule.prototype.findBlockEnd = function(citem){
	if(citem == null){//if citem is unset
		citem = this.channel.modules.playlist.current.next;//set citem to current item by default
		if(citem == null){//return current item if you're at end of list
			return this.channel.modules.playlist.current;
		}
	}
	var og = citem;
	while(citem != null && citem.media.isBump){//while a bump is next, and we haven't hit the end of the line
		if(citem.next == null){//if we hit end of the list
			return citem;//stahp, and return what we got.
		}else{
			citem = citem.next;//move downt the playlist
		}
	}

	return citem.prev;//return last bump of block, return first item if no bumps
};

//Bump Queueing/
AutobumpModule.prototype.queueRandom = function queueRandom(alist){
	if(alist == null){
		alist = [];
		this.listsel().forEach(function(bump){//go through selected list
			if(bump != null && !bump.noauto){//for every item that isnt null
				alist.push(bump);//add it to alist 
			}
		});
	}
	
	var chosen = alist[randi(alist.length)];

	if(chosen != null){
		/*(if(this.channel.modules.playlist.items.findVideoId(chosen.media.id) != false){//dupe detector dupes are an accepted casualty :P
			this.queueRandom(alist);
		}*/
		this.queueBump(chosen.listname, chosen.id);
	}
};


AutobumpModule.prototype.roundRobin = function roundRobin(){
	var alist = [];//create alist var
	this.listsel().forEach(function(bump){//go through selected list
		if(bump != null && !bump.noauto){//for every item that isnt null
			alist.push(bump);//add it to alist 
		}
	});

	var curbump = null;//define curbump

	var rhist = this.lastPlayed.slice().reverse();//create reverse history array
	
	var curin = null;
	rhist.forEach(function(bump, i){//go through bump history, starting with the newest bumps
		if(alist.includes(bump) && curin == null){//if we got a match
			curin = alist.findIndex(function(cbump){//get its index
				return (cbump.id == bump.id && cbump.listname === bump.listname);
			});
		}
	}); 
	if(curin != null){//if we found an index
		if(alist.length <= curin + 1){//if we hit the end of the list
			this.queueBump(alist[0].listname, alist[0].id, true);//wrap around list and queue from beggining
		}else{//otherwise
			this.queueBump(alist[curin + 1].listname, alist[curin + 1].id, true);//queue next bump
		}
	}else if(alist[0] != null){//or if we didnt
			this.queueBump(alist[0].listname, alist[0].id, true);//assume we haven't played anything and play the first bump in the list.
	}

};

AutobumpModule.prototype.lastHalfRandom = function lastHalfRandom(){
	var alist = this.listsel();
	var inhist = [];//filtered bump history containing only relavent bumps
	var selbumps = [];//bumps elligible for selection
	var alength = 0;//length of alist without null values

	this.lastPlayed.forEach(function(bump){//for every bump in history
		if(alist.includes(bump) && bump != null && !bump.noauto){//if that bump is in the list of bumps created by the listsel method
			inhist.push(bump);//add it to the inhist array
		}
	});
	
	alist.forEach(function(bump){
		if(bump != null && !bump.noauto)
			alength++;
	});
	if(inhist.length >= alength){//if all active bumps have been played
		selbumps = inhist.slice(0,Math.round(inhist.length / 2));//set eligible bumps to the least recently played half of all active bumps
	}else{//if there are unplayed active bumps
		alist.forEach(function(bump){//for every active bump
			if(!inhist.includes(bump) && bump != null && !bump.noauto){//if it hasn't been played
				selbumps.push(bump);//add it to selbumps
			}
		});
	}

	if(selbumps.length > 0){//if we got somethin
		this.queueRandom(selbumps);//then throw it at the random queue method
	}
}

AutobumpModule.prototype.handleQueueBump = function(user, data){
	if(user != null && user.account.effectiveRank >= 2){
		this.queueBump(XSS.sanitizeText(data.bl), data.id);
	}
};

AutobumpModule.prototype.autobump = function(){
	var bcount = randrange(this.bumpFreq[0], this.bumpFreq[1]);
	if(this.channel.modules.playlist.current.media.seconds > this.minBump || this.channel.modules.playlist.current.media.isBump){//if current item is long enough to have a bump (make an exception for bumps, so that we can queue infinite bumps at playlist end)
		for(var i = 0; i < bcount; i++){//for bcount
			this.selmed();//queue bump via selmed
		}
	}
}

AutobumpModule.prototype.queueBump = function(listn, bid){//listname, bump ID
		var list = bumplists.get(listn.toLowerCase().replace(lowerReg, ""))
		var bump = null;
		var data = null;
		var _this = this;
		var pbump = 'next';

		if(list != null){//if bumplist exists
			bump = list.bumps[bid]

			var lastin = this.lastPlayed.findIndex(function(cbump){
				return (cbump != null && bump != null && cbump.id == bump.id && cbump.listname === bump.listname);
			});

			if(this.lastPlayed[lastin] != null){
				this.lastPlayed.splice(lastin,1);
			}


			this.lastPlayed.push(bump)
			this.channel.users.forEach(function(user){	
				_this.sendHist(user);
			});

			if(this.channel.modules.playlist.current != null && this.channel.modules.playlist.current.next != null && this.channel.modules.playlist.current.next.media.isBump){
				pbump = this.findBlockEnd(this.channel.modules.playlist.current.next).uid;
			}



			if(bump != null){
				data = { //create faux data object for _addItem function
					id: bump.media.id,
					type: bump.media.type,
					pos: pbump,
					title: false,
					subtitle: false,
					link: util.formatLink(bump.media.id, bump.media.type),
					temp: true,
					shouldAddToLibrary: false,
					queueby: 'autobump',
					maxlength: 0
				}


    				var nmed = new Media(bump.media.id, bump.media.title, bump.media.seconds, bump.media.type, bump.media.meta);//revive media object, should probably do this when parsing JSON but fuck you this works :P
				nmed.isBump = true;
				nmed.tokeBump = bump.rtoke;

				this.channel.modules.playlist._addItem(nmed, data, "autobump", null, true);

			}
		}
};

//list Selection
AutobumpModule.prototype.smashList = function smashList(){
	var alist = [];
	
	
	this.activeLists.forEach(function (bl){
		if(bl.bumps != null){
			bl.bumps.forEach(function(bump){
				alist.push(bump);
			});
		}
	});
	return alist;
};

AutobumpModule.prototype.randomList = function randomList(){
	var llist = [];
	this.activeLists.forEach(function (bl){
		llist.push(bl);
	});

	var clist = llist[randi(llist.length)];
	if(clist != null){
		return clist.bumps;
	}else{
		return [];
	}

};

//configuration methods 
AutobumpModule.prototype.loadConfig = function(_this){
	if(_this == null){
		var _this = this;
	}
	
	fs.readFile(configFolder + _this.channel.name + ".conf", function(err,rdata){
		var defAgro = 2;
		var defFreq = [1,3];
		var defBSort = "lastHalfRandom";
		var defLSort = "smashList";
		var defMin = 240;


		if(err){
			console.log("[Autobump Config]  " + err);

			_this.agro =  defAgro ;
			_this.bumpFreq = defFreq;
			_this.minBump = defMin;
			_this.selmed = _this.lastHalfRandom;
			_this.listsel = _this.smashList;

			return;
		}

		var data = JSON.parse(rdata);

		data.active == null ? [] : data.active;
		if(data.active != null){
			data.active.forEach(function(al){
				if(bumplists.get(al) != null){
					_this.activeLists.set(al, bumplists.get(al));
				}
			});
		}
		_this.agro = data.agro == null ? defAgro : data.agro;
		_this.bumpFreq = data.freq == null ? defFreq : data.freq;
		_this.minBump = data.minBump == null ? defMin : data.minBump;
		_this.selmed = data.bsort == null ? _this.lastHalfRandom : _this[data.bsort];
		_this.listsel = data.lsort == null ? _this.smashList : _this[data.lsort];
	});		


};

AutobumpModule.prototype.saveConfig = function(){
	var _this = this;
	var confObj = {
		active: [],
		agro: this.agro,
		freq: this.bumpFreq,
		bsort: this.selmed.name,
		lsort: this.listsel.name,
		minBump: this.minBump
	}

	this.activeLists.forEach(function(bumplist){
		confObj.active.push(bumplist.lowername);
	});


	fs.writeFile(configFolder + this.channel.name + ".conf", JSON.stringify(confObj), function(err,data){
		if(err){
			console.log("[Autobump Config]  " + err);
			return;
		}	

		_this.channel.users.forEach(function(user){	
			_this.sendConf(user)
		});
	});		

}

AutobumpModule.prototype.handleSetSelect = function(user, data){
	if(user != null && user.account.effectiveRank >= 2){
		switch(data.list){
			case 1:
				this.listsel = this.smashList;
				break;
			default:
				this.listsel = this.randomList;
		}
		switch(data.bump){
			case 1:
				this.selmed = this.queueRandom;
				break;
			case 2:
				this.selmed = this.roundRobin;
				break;
			default:
				this.selmed = this.lastHalfRandom;
		}
	}

	this.saveConfig();
};

AutobumpModule.prototype.handleSetAgro = function(user, data){
	if(user != null && user.account.effectiveRank >= 2){
		data = data > 3 ? 3 : data;
		this.agro = data < 0 ? 0 : data;
	}

	this.saveConfig();
}

AutobumpModule.prototype.handleSetBumpFreq = function(user, data){
	if(user != null && user.account.effectiveRank >= 2){
		data.min = Math.abs(data.min);//set values to absolute
		data.max = Math.abs(data.max);

		this.bumpFreq[0] = data.min;
		this.bumpFreq[1] = data.min < data.max ? data.max : data.min;//if min is more then max then max will be min
	}

	this.saveConfig();
};

AutobumpModule.prototype.handleSetMinBump = function(user, data){
	if(user != null && user.account.effectiveRank >= 2){
		this.minBump = Math.abs(data);

		this.saveConfig();
	}
};

//list management/commands
AutobumpModule.prototype.handleSetActive = function(user, data){
	if(user != null && user.account.effectiveRank >= 2){
		var bl = bumplists.get(XSS.sanitizeText(data).toLowerCase().replace(lowerReg, ""));
		if(bl != null){//if bumplist exists
			if(this.activeLists.get(XSS.sanitizeText(data).toLowerCase().replace(lowerReg, "")) != null){
				console.log("list already active");
				return;	
			}
			this.activeLists.set(XSS.sanitizeText(data).toLowerCase().replace(lowerReg, ""), bl);
		}

		this.saveConfig();
	}
};

AutobumpModule.prototype.handleRemoveActive = function(user, data){
	if(user != null && user.account.effectiveRank >= 2){
		if(this.activeLists.has(XSS.sanitizeText(data).toLowerCase().replace(lowerReg, ""))){
			this.activeLists.delete(XSS.sanitizeText(data).toLowerCase().replace(lowerReg, ""));		
		}else{
			console.log("list not active!")
		}
		this.saveConfig();
	}
};

AutobumpModule.prototype.sendLists = function(user, data){
	if(user != null && user.account.effectiveRank >= 2){
		var sendobj = {
			lists: [],
			active: [],
			freq: this.bumpFreq,
			agro: this.agro,
			minBump: this.minBump,
			history: [],
			bsort: this.selmed.name,
			lsort: this.listsel.name
		}

		bumplists.forEach(function(bumplist){
			sendobj.lists.push(bumplist.packList());
		});

		this.activeLists.forEach(function(bumplist, key){
			sendobj.active.push(bumplist.lowername);
		});

		this.lastPlayed.forEach(function(bump){
			sendobj.history.push([bump.listname, bump.id]);
			
		});

		user.socket.emit("sendBumplists", sendobj);
	}
};

AutobumpModule.prototype.sendList = function(user, data, oname){
	if(user != null && user.account.effectiveRank >= 2){
		var list = bumplists.get(data.toLowerCase().replace(lowerReg, ""))
		if(list != null){
			var pack = list.packList();
			if(oname != null){
				pack.oname = oname;
			}
			user.socket.emit("sendBumplist", pack);
		}
	}
};

AutobumpModule.prototype.sendHist = function(user){
	if(user != null && user.account.effectiveRank >= 2){
		var sendobj = [];
		this.lastPlayed.forEach(function(bump){
			sendobj.push([bump.listname, bump.id]);
			
		});
		user.socket.emit("sendBumphist", sendobj);
	}
}

AutobumpModule.prototype.sendConf = function(user){
	if(user != null && user.account.effectiveRank >= 2){
		var sendobj = {
			active: [],
			freq: this.bumpFreq,
			agro: this.agro,
			minBump: this.minBump,
			bsort: this.selmed.name,
			lsort: this.listsel.name
		}

		this.activeLists.forEach(function(bumplist, key){
			sendobj.active.push(bumplist.lowername);
		});

		user.socket.emit("sendBumpconf", sendobj);
	}
}

AutobumpModule.prototype.handleNewBumplist = function(user, data){//handle newBumplist
	if(user.account.effectiveRank >= 2){
		if(bumplists.get(XSS.sanitizeText(data).toLowerCase().replace(lowerReg, "")) == null){
			bumplists.set(XSS.sanitizeText(data).toLowerCase().replace(lowerReg, ""), new bumplist(XSS.sanitizeText(data)));//create new bumplist

			this.sendList(user,XSS.sanitizeText(data).toLowerCase().replace(lowerReg, ""));

		}else{
			user.socket.emit("errorMsg", {
				msg: "Bumplist name taken: " + XSS.sanitizeText(data).toLowerCase().replace(lowerReg, ""),
				alert: true
			});
			return;
		}
	}
};

AutobumpModule.prototype.handleCloneBumplist = function(user, data){//handle newBumplist
	if(user.account.effectiveRank >= 2){
		if(bumplists.get(XSS.sanitizeText(data).toLowerCase().replace(lowerReg, "")) == null){
			var cbumps = [];
			this.channel.modules.playlist.items.forEach(function(item){
				var cbump = new bump(item.media.title, null, false, item.media, null, false);
				cbump.listname = XSS.sanitizeText(data).toLowerCase().replace(lowerReg, "");
				cbump.id = cbumps.length;
				cbumps.push(cbump);
			});
			
			var blist =  new bumplist(XSS.sanitizeText(data), cbumps)
			
			bumplists.set(blist.lowername, blist);//create new bumplist
			blist.saveList();
		}else{
			user.socket.emit("errorMsg", {
				msg: "Bumplist name taken: " + XSS.sanitizeText(data).toLowerCase().replace(lowerReg, ""),
				alert: true
			});
			return;
		}
	}
}

AutobumpModule.prototype.handleRenameBumplist = function(user, data){//handle newBumplist
	if(user.account.effectiveRank >= 2){
		if(bumplists.get(XSS.sanitizeText(data.oldname).toLowerCase().replace(lowerReg, "")) != null){
			var active = false;
			var _this = this;
			var list = bumplists.get(XSS.sanitizeText(data.oldname).toLowerCase().replace(lowerReg, ""));
			if(active = this.activeLists.get(XSS.sanitizeText(data.oldname).toLowerCase().replace(lowerReg, "")) != null)
				this.activeLists.delete(XSS.sanitizeText(data.oldname).toLowerCase().replace(lowerReg, ""));
			
			list.rename(XSS.sanitizeText(data.oldname), XSS.sanitizeText(data.newname), function(){
				if(active){
					_this.activeLists.set(XSS.sanitizeText(data.newname).toLowerCase().replace(lowerReg, ""), list);
					_this.saveConfig();
				}
			});

				
		}else{
				user.socket.emit("errorMsg", {
				msg: "Non-Existant Bumplist: " + XSS.sanitizeText(data.oldname).toLowerCase().replace(lowerReg, ""),
				alert: true
			});
		}
	}
}

AutobumpModule.prototype.handleDelBumplist = function(user, data){//handle newBumplist
	if(user.account.effectiveRank >= 2){
		var active = false;
		if(bumplists.get(XSS.sanitizeText(data).toLowerCase().replace(lowerReg, "")) != null){
			if(active = this.activeLists.get(XSS.sanitizeText(data).toLowerCase().replace(lowerReg, "")) != null)
				this.activeLists.delete(XSS.sanitizeText(data).toLowerCase().replace(lowerReg, ""));

			bumplists.get(XSS.sanitizeText(data).toLowerCase().replace(lowerReg, "")).deleteList();
		}else{
			user.socket.emit("errorMsg", {
				msg: "Non-Existant Bumplist: " + XSS.sanitizeText(data).toLowerCase().replace(lowerReg, ""),
				alert: true
			});
			return;
		}
	}
};

//bump management/commands
AutobumpModule.prototype.handleNewBump = function(user, data){//handle newBump 
	if(user.account.effectiveRank >= 2){
		console.log("Pulling bump info...");
		InfoGetter.getMedia(data.id, data.type, function(err, media){//get media
			if(err){//error handling
				user.socket.emit("errorMsg", {
					msg: "Error pulling bump: " + err,
					alert: true
				});
				return;
			}
			if(data.user === ""){
				data.user = null;
			}else{
				data.user = XSS.sanitizeText(data.user);
			}
			var newbump = new bump(XSS.sanitizeText(data.name), data.user, data.rtoke, media, XSS.sanitizeText(data.bumplist), data.noauto);//create new bump
		});
	}
};

AutobumpModule.prototype.handleRenameBump = function(user, data){
	if(user.account.effectiveRank >= 2){
		if(bumplists.get(XSS.sanitizeText(data.bl)) != null && bumplists.get(XSS.sanitizeText(data.bl)).bumps[data.id] != null){
			bumplists.get(XSS.sanitizeText(data.bl)).bumps[data.id].rename(XSS.sanitizeText(data.name));
			bumplists.get(XSS.sanitizeText(data.bl)).saveList();

		}
	}
}

AutobumpModule.prototype.handleChangeCreator = function(user, data){
	if(user.account.effectiveRank >= 2){
		if(bumplists.get(XSS.sanitizeText(data.bl)) != null && bumplists.get(XSS.sanitizeText(data.bl)).bumps[data.id] != null){
			bumplists.get(XSS.sanitizeText(data.bl)).bumps[data.id].user = (XSS.sanitizeText(data.name));
			bumplists.get(XSS.sanitizeText(data.bl)).saveList();
		}
	}
}

AutobumpModule.prototype.handleToggleNoauto = function(user, data){
	if(user.account.effectiveRank >= 2){
		if(bumplists.get(data.bl) != null && bumplists.get(data.bl).bumps[data.id] != null){
			bumplists.get(data.bl).bumps[data.id].noauto = !bumplists.get(data.bl).bumps[data.id].noauto;
			bumplists.get(data.bl).saveList();
		}
	}
}

AutobumpModule.prototype.handleToggleRtoke = function(user, data){
	if(user.account.effectiveRank >= 2){
		if(bumplists.get(data.bl) != null && bumplists.get(data.bl).bumps[data.id] != null){
			bumplists.get(data.bl).bumps[data.id].rtoke = !bumplists.get(data.bl).bumps[data.id].rtoke;
			bumplists.get(data.bl).saveList();
		}
	}
}

AutobumpModule.prototype.handleDeleteBump = function(user, data){
	if(user.account.effectiveRank >= 2){
		var bl = bumplists.get(XSS.sanitizeText(data.bl));
		if(bl != null){
			bl.deleteBump(bl.bumps[data.id]);
		}else{
			user.socket.emit("errorMsg", {
				msg: "Bumplist: " + XSS.sanitizeText(data.bl) + " not found!",
				alert: true
			});
		}
	}
};

module.exports = AutobumpModule;
