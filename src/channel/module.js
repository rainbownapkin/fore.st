/*
fore.st is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

fore.st is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with fore.st. If not, see < http://www.gnu.org/licenses/ >.
(C) 2022- by rainbownapkin, <ourforest@420blaze.it>

Original cytube license:
MIT License

Copyright (c) 2013-2022 Calvin Montgomery

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
function ChannelModule(channel) {
    this.channel = channel;
    this.dirty = false;
    this.supportsDirtyCheck = false;
}

ChannelModule.prototype = {
    /**
     * Called when the channel is loading its data from a JSON object.
     */
    load: function (_data) {
    },

    /**
     * Called when the channel is saving its state to a JSON object.
     */
    save: function (_data) {
    },

    /**
     * Called when the channel is being unloaded
     */
    unload: function () {

    },

    /**
     * Called to pack info, e.g. for channel detail view
     */
    packInfo: function (_data, _isAdmin) {

    },

    /**
     * Called when a user is attempting to join a channel.
     *
     * data is the data sent by the client with the joinChannel
     * packet.
     */
    onUserPreJoin: function (_user, _data, cb) {
        cb(null, ChannelModule.PASSTHROUGH);
    },

    /**
     * Called after a user has been accepted to the channel.
     */
    onUserPostJoin: function (_user) {
    },

    /**
     * Called after a user has been disconnected from the channel.
     */
    onUserPart: function (_user) {
    },

    /**
     * Called when a chatMsg event is received
     */
    onUserPreChat: function (_user, _data, cb) {
        cb(null, ChannelModule.PASSTHROUGH);
    },

    /**
     * Called before a new video begins playing
     */
    onPreMediaChange: function (_data, cb) {
        cb(null, ChannelModule.PASSTHROUGH);
    },

    /**
     * Called when a new video begins playing
     */
    onMediaChange: function (_data) {

    },
     /**
     * Called when media is moved
     */
    onMediaMove: function (_data) {

    },
    /**
     * Called when media is added to the playlist
     */
    onMediaAdd: function (_data, _media) {

    }
};

/* Channel module callback return codes */
ChannelModule.ERROR = -1;
ChannelModule.PASSTHROUGH = 0;
ChannelModule.DENY = 1;

module.exports = ChannelModule;
